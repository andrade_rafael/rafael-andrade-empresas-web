import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { EnterprisesComponent } from './enterprises/enterprises.component';
import { LoginComponent } from './login/login.component';
import { EnterpriseDetailsComponent } from './enterprises/enterprise-details/enterprise-details.component'
import { EnterpriseItemComponent } from "./enterprises/enterprise-item/enterprise-item.component"


import { EnterprisesService } from "./enterprises/enterprises.service";
import { LoginService } from './login/login.service';


@NgModule({
  declarations: [
    AppComponent,
    EnterprisesComponent,
    LoginComponent,
    EnterpriseDetailsComponent,
    EnterpriseItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [LoginService,
  EnterprisesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
