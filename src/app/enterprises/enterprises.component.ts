import { Component, OnInit } from '@angular/core';

import { Enterprise } from './enterprise.model';


// Services
import { EnterprisesService } from './enterprises.service';
import {Subscription} from 'rxjs/Subscription';
@Component({
  selector: 'app-enterprises',
  templateUrl: './enterprises.component.html',
  styleUrls: ['./enterprises.component.css']
})
export class EnterprisesComponent implements OnInit {
  private value: string;
  private dadosCarregados = false;
  private subscription: Subscription;
  private searching = false;
  private enterprises: Enterprise[] = [];

  constructor(
    private enterpriseService: EnterprisesService,
  ) { }

  ngOnInit() {  }

  // Consome rota de pesquisa - somente pelo nome
  pesquisar (value: string) {
    console.log(value);
    this.enterpriseService.seach(value).subscribe(resp => {
      this.enterprises = resp.json().enterprises;
      this.dadosCarregados = true;
    });
  }


  // search bar is on
  search() {
    this.searching = true;
  }


  // search bar is off
  cancel () {
    this.searching = false;
  }
}
