import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
// Models
import { Enterprise } from './enterprise.model';

//Services
import { LoginService } from '../login/login.service';

// Configuration file
import { config } from "../config/config";
import {Params} from "@angular/router";


@Injectable()
export class EnterprisesService {
  private enterprises: Enterprise[] = [];

  public loaded: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor (
    private http: Http,
    private loginService: LoginService
  ) {
  }

  seach(nome: string) {

    const headers = new Headers({
      'Content-type': 'application/json',
      'access-token': this.loginService.getToken(),
      'client': this.loginService.getClient(),
      'uid': this.loginService.getUid()
    });

    let params: URLSearchParams = new URLSearchParams();
    params.set('name' , nome);
    console.log(params);


    const options = new RequestOptions({headers: headers, params: params });


    options.params = params;
    return this.http.get(config.hostname + ':' + config.port +
      '/api/' + config.version + '/enterprises', options);

  }

  load (id: number) {
    const headers = new Headers({
      'Content-type': 'application/json',
      'access-token': this.loginService.getToken(),
      'client': this.loginService.getClient(),
      'uid': this.loginService.getUid()
    });


    const options = new RequestOptions({headers: headers});

    return this.http.get(config.hostname + ':' + config.port +
      '/api/' + config.version + '/enterprises/' + id, options);

  }

}
