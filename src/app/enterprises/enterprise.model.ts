export class Enterprise {
  public id: number;
  public email_enterprise: string;
  public facebook: string;
  public twitter: string;
  public linkedin: string;
  public phone: string;
  public own_enterprise: boolean;
  public enterprise_name: string;
  public photo: string;
  public description: string;
  public city: string;
  public country: string;
  public value: number;
  public share_price: 5000;
  public enterprise_type: {
    id: number;
    enterprise_type_name: string
  };
}
