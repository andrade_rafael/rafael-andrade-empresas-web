import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";

import {EnterprisesService} from "../enterprises.service";
import {Enterprise} from "../enterprise.model";


import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'app-enterprise-details',
  templateUrl: './enterprise-details.component.html',
  styleUrls: ['./enterprise-details.component.css']
})
export class EnterpriseDetailsComponent implements OnInit {

  private enterprise: Enterprise;
  private dadosCarregados = false;

  constructor(private route: ActivatedRoute,
              private enterpriseService: EnterprisesService) {
  }


  /* Initialize data with URL params
  * API enterprise/:id */
  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => this.enterpriseService.load(+params['id']))
        .subscribe(resp => {
          this.enterprise = resp.json().enterprise;
          console.log(JSON.stringify(this.enterprise))
          this.dadosCarregados = true;
        } , (err) => {
          console.log(err);
        });
  }

}
