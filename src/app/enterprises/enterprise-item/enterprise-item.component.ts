import {Component, Input, OnInit} from '@angular/core';

import { Enterprise } from "../enterprise.model"
import {Router} from "@angular/router";

@Component({
  selector: 'app-enterprise-item',
  templateUrl: './enterprise-item.component.html',
  styleUrls: ['./enterprise-item.component.css']
})
export class EnterpriseItemComponent implements OnInit {
  @Input() enterprises: Enterprise[]

  constructor(
    private router: Router) {
  }

  ngOnInit() {
  }


  // API route enterprises/ID
  showEnterprise(id: number) {
    this.router.navigate(['empresas', id]);
  }

}
