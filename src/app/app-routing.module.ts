import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EnterprisesComponent } from './enterprises/enterprises.component';
import { EnterpriseDetailsComponent } from "./enterprises/enterprise-details/enterprise-details.component"

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'empresas', component: EnterprisesComponent },
  { path: 'empresas/:id', component: EnterpriseDetailsComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
})

export class AppRoutingModule {}
