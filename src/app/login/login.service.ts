import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { config } from '../config/config';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';



/* TODO auth service + localstorage*/


@Injectable()
export class LoginService {
  private token;
  private uid;
  private client;
  public loaded: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor (
    private http: Http,
  ) {
  }

  /* TODO auth service*/
  sign_in(email: string, password: string) {

    const headers = new Headers({'Content-type': 'application/json'});
    const options = new RequestOptions({ headers: headers });

    const data: any = { 'email': email, 'password': password };

    this.http.post(config.hostname + ':' + config.port +
      '/api/' + config.version + '/users/auth/sign_in', data, options).subscribe(resp => {
      if (resp.json().success) {
        this.uid = resp.headers.get('uid');
        this.client = resp.headers.get('client');
        this.token = resp.headers.get('access-token');
        this.loaded.next(true);
      }
    }, () => console.log('error'));
  }


  getToken() { return this.token; }

  getUid () { return this.uid; }

  getClient() { return this.client; }


}
