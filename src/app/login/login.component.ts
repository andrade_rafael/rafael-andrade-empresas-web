import { Component, OnInit } from '@angular/core';

import { LoginService } from './login.service';
import {Subscription} from "rxjs/Subscription";
import { Router } from "@angular/router"

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private email: string;
  private password: string;
  private subcription: Subscription;
  private loading: boolean = false;

  constructor(
    private loginService: LoginService,
    private router: Router) { }

  ngOnInit() {
  }

  login() {

    this.loading = true;
    this.loginService.sign_in(this.email, this.password);
    this.subcription = this.loginService.loaded.subscribe(logged_in => {
      if (logged_in) {
        this.router.navigate(['/empresas']);
        this.loading = false;
        this.subcription.unsubscribe();
      }
    });

  }




}
