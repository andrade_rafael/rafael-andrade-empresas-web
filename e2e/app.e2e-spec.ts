import { IoasysWebPage } from './app.po';

describe('ioasys-web App', () => {
  let page: IoasysWebPage;

  beforeEach(() => {
    page = new IoasysWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
